from django import forms
from .models import Matkul, Delete, Kegiatan, Orang
from django.forms import ModelForm
class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'

class DeleteForm(ModelForm):
    class Meta:
        model = Delete
        fields = '__all__'

class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"

class OrangForm(ModelForm):
    class Meta:
        model = Orang
        fields = ["nama_orang"]