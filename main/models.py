from django.db import models

# Create your models here.
class Matkul(models.Model):
    matkul_name = models.CharField(max_length=50)
    lecturer_name = models.CharField(max_length=50)
    sks = models.IntegerField()
    matkul_desc =  models.TextField(max_length=100)
    acad_year = models.CharField(max_length=50)
    room = models.CharField(max_length=10)

class Delete(models.Model):
    del_name = models.CharField(max_length=50)

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    deskripsi_kegiatan = models.TextField(max_length=100)
    def __str__(self):
        return self.nama_kegiatan

class Orang(models.Model):
    nama_kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama_orang = models.CharField(max_length=50)
    def __str__(self):
        return self.nama_orang