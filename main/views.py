from django.shortcuts import render, redirect
from .forms import MatkulForm, DeleteForm, KegiatanForm, OrangForm
from django.http import HttpResponse, HttpResponseRedirect
from .models import Matkul, Delete, Kegiatan, Orang

# Create your views here.
def index(request):
    
    return render(request, 'main/index.html')
def write(request):
    return render(request, 'main/write.html')
def about(request):
    return render(request, 'main/about.html')
def story1(request):
    return render(request, 'main/story1.html')
def edit(request):
    if(request.method == 'POST' ):
        form = MatkulForm(request.POST)
        if(form.is_valid()):
            
            name = form.cleaned_data['matkul_name']
            lecturer = form.cleaned_data['lecturer_name']
            sks = form.cleaned_data['sks']
            year = form.cleaned_data['acad_year']
            desc = form.cleaned_data['matkul_desc']
            room = form.cleaned_data['room']
            p = Matkul(matkul_name=name, lecturer_name=lecturer, sks=sks, acad_year=year, matkul_desc=desc, room=room)
            p.save()
            return HttpResponseRedirect('/success')
    else:
        form = MatkulForm
    context = {'form' : form}
    return render(request, 'main/editmatkul.html', context)

def matkul(request):
    matkullist=Matkul.objects.all().values()
    return render(request, 'main/matkul.html', {'matkullist':matkullist})

def success(request):
    return render(request, 'main/success.html')

def delete(request):
    if(request.method == 'POST' ):
        form = DeleteForm(request.POST)
        if(form.is_valid()):
            name_tobe_del = form.cleaned_data['del_name']
            id_tobe_del = 0
            for dc in Matkul.objects.all().values():
                if dc.get("matkul_name").strip().lower() == name_tobe_del.strip().lower():
                    id_tobe_del = dc.get("id")

            if id_tobe_del != 0:
                Matkul.objects.filter(id=id_tobe_del).delete() 
            return HttpResponseRedirect('/success')
    else:
        form = DeleteForm
    context = {'form' : form}
    return render(request, 'main/delete.html', context)



def detail(request):
    if request.method=='GET':
        iid = request.GET.get('id')
        m=""
        d=""
        for dc in Matkul.objects.all().values():
                if int(dc.get("id")) == int(iid):
                    d = dc.get("matkul_desc")
                    m = dc.get("matkul_name")
        # if not m:
        #     return render(request, 'another.html')
        
        return render(request, 'main/detail.html', {'m':m, 'd':d})

def tambahkegiatan(request):
    if(request.method == "POST"):
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/success')
    else:
        form = KegiatanForm()
    context = {'form' : form}
    return render(request, "main/tambahkegiatan.html", context)

def kegiatan(request):
    orang = Orang.objects.all()
    kegiatan = Kegiatan.objects.all()
    context = {"orang":orang, "kegiatan":kegiatan}
    return render(request, 'main/kegiatan.html', context)

def daftar(request, id):
    if(request.method == "POST"):
        form = OrangForm(request.POST)
        if form.is_valid():
            kegiatan = Kegiatan.objects.get(id=id)
            orang = form.cleaned_data["nama_orang"]
            form = Orang(nama_kegiatan = kegiatan, nama_orang=orang)
            form.save()
            return HttpResponseRedirect('/success')
    else:
        form = OrangForm()
    context = {'form' : form}
    return render(request, "main/daftar.html", context)