from django.test import TestCase, Client
from django.urls import resolve
from .views import index, daftar, tambahkegiatan, kegiatan
from .models import Kegiatan, Orang

# Create your tests here.
class TestIndex(TestCase):
	def test_index_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, kegiatan)

	def test_index_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'main/kegiatan.html')

class TestTambahKegiatan(TestCase):
	def test_add_Event_url_is_exist(self):
		response = Client().get('/tambahkegiatan')
		self.assertEqual(response.status_code, 200)

	def test_add_Event_index_func(self):
		found = resolve('/tambahkegiatan')
		self.assertEqual(found.func, tambahkegiatan)

	def test_Event_model_create_new_object(self):
		acara = Kegiatan(nama_kegiatan ="abc", deskripsi_kegiatan="cde")
		acara.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

class TestAddMember(TestCase):
	def setUp(self):
		acara = Kegiatan(nama_kegiatan ="abc", deskripsi_kegiatan="cde")
		acara.save()

	def test_add_Member_url_is_exist(self):
		response = Client().post('/daftar/1', data={'nama':'aziz'})
		self.assertEqual(response.status_code, 200)

class TestModel(TestCase):
	def test_str_model_event(self):
		event = Kegiatan.objects.create(nama_kegiatan='pepewe', deskripsi_kegiatan='ppwisfun')
		self.assertEqual(event.__str__(), 'pepewe')

	def test_str_model_member(self):
		event = Kegiatan.objects.create(nama_kegiatan='pepewe', deskripsi_kegiatan='ppwisfun')
		member = Orang.objects.create(nama_orang="memberTest", nama_kegiatan=event)
		self.assertEqual(member.__str__(), 'memberTest')