from django.contrib import admin

# Register your models here.
from .models import Matkul, Kegiatan, Orang
admin.site.register(Matkul)
admin.site.register(Kegiatan)
admin.site.register(Orang)
